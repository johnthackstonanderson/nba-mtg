<?php $version = 3; ?>
<!DOCTYPE html>
<html>
    <head>
    <title>Magical Basketball Association</title>
         <meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, shrink-to-fit=no" name="viewport" />


    <meta property="twitter:title" content="Magical Basketball Association" />
    <meta property="twitter:description" content="An NBA themed Magic: The Gathering set, made by a fan of both" />
    

    <meta property="og:url" content="http://magicthebasketballing.com" />
    <meta property="og:type" content="image" />
    <meta property="og:title" content="Magical Basketball Association" />
    <meta property="og:description" content="An NBA themed Magic: The Gathering set, made by a fan of both" />
    <meta property="og:image" content="http://magicthebasketballing.com/nbacube/img/logo.png" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="226" />
    <meta property="og:image:height" content="463" />

        <link rel="icon shortcut"  type="image/png" href="http://magicthebasketballing.com/nbacube/img/favicon.png?v=<?php $version; ?>"/>
        <style>
            html {
                box-sizing:border-box;
                font-size:16px;
                font-family:roboto,sans-serif;
                font-weight:300;
            }
            body {
                overflow:scroll;
                height:auto;
            }
            .bg {
                z-index:0;
                width:100vw;
                height:100vh;
                position:fixed;
                top:0;
                left:0;
                background-image:url(./nbacube/img/bg.jpg);
               
                background-repeat:no-repeat;
                background-size:cover;
                background-position:center center;
            }
            .bg:after  {
                position: absolute;
                z-index: 1;
                width: 100%;
                height: 100%;
                display: block;
                left: 0;
                top: 0;
                content: "";
                background: rgba(32, 2, 120, 0.3);
            }
            .main {
                z-index:1;
                position:relative;
                max-width:1400px;
                margin:auto;
            }
            .cards {
                display:flex;
                flex-flow:row wrap;
                align-content:flex-start;
                justify-content:flex-start;
            }
            .card{
                flex: 0 0 25%;
            }
            .header {
                background: rgba(0,0,0,.3);
                padding:1rem;
            }
            .header a {
                color:white;
                text-align:center;
                display:block;
            }
            h1 {
                text-align:center;
                font-size:6rem;
                color:white;
            }
             h4 {
                text-align:center;
                font-size:2rem;
                color:white;
            }
        </style>
        <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-47034783-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-47034783-3');
</script>

    </head>
    <body>
        <div class="bg">
             </div>
             <div class="main">
        <div class="header">
            <h1><img height="auto" width="100%" src="./nbacube/img/wordmark.svg?v=<?php $version; ?>"/></h1>
            <h4>I'm a nerd who loves MTG and the NBA</h4>
            <a href="https://bitbucket.org/johnthackstonanderson/nba-mtg/src/master/">Git Repository</a>

        </div>
        <div class="cards">
        <?php 
            $files = scandir ( __DIR__."/nbacube/cards",SCANDIR_SORT_ASCENDING);
            foreach($files as $file) {
                if(strpos($file,"jpg") !== false){
                    echo "<div class='card'><img width='100%' src='./nbacube/cards/".$file."?v=$version'/ /></div>";
                }
            }

        ?></div>
     
       
        <script type="text/javascript" src="./nbacube/js/nbamtg.js?v=<?php $version; ?>"></script>
    </body>
</html>